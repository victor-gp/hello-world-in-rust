# MLH Local Hack Day: Build - Hello World

## Inspiration

I've been planning to learn Rust for a while now, there's a great momentum behind it.

I figured out that I might as well walk my first steps with Local Hack Build and earn an Ellie sticker!

## What it does

Prints "Hello world!", then "Hello MLH!".

## How I built it

Copypasting from a Rust tutorial. Tweaking it a bit.

## What I learned

The people who made Rust seem to like abbreviatures (fn).

Somehow, semicolons are still a thing in modern languages. :P

## What's next for Hello World in Rust

Actually learning Rust.
